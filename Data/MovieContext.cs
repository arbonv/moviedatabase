﻿using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.EntityFrameworkCore;
using MovieDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDatabase.Data
{
    public class MovieContext : DbContext
    {
        public MovieContext(DbContextOptions<MovieContext> options)
            : base(options)
        {
        }

        public DbSet<Film> Films
        {
            get; set;

        }

        public DbSet<Genre> Genres
        {
            get;
            set;
        }

        public DbSet<Director> Director
        {
            get; set;
        }
        public DbSet<User> User { get; set; }

        public DbSet<Star> Stars
        {
            get; set;
        }



        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>().ToTable("Genres");

            modelBuilder.Entity<Film>()
                .ToTable("Film")
                .HasDiscriminator<int>("FilmType")
                .HasValue<Movie>(1)
                .HasValue<TvShow>(2);

            modelBuilder.Entity<Film>().HasKey(f => new { f.FilmId });
            

            modelBuilder.Entity<Star>()
                 .ToTable("Stars");
#if false
            modelBuilder.Entity<Star>().HasKey(s => new { s.Film.FilmId });
            modelBuilder.Entity<Director>().HasKey(d => new { d.FilmId }); 
            modelBuilder.Entity<Genre>().HasKey(g => new { g.Film.FilmId });
#endif
            modelBuilder.Entity<User>().ToTable("Users");
 
            modelBuilder.Entity<Director>().ToTable("Directors");

            //Film<-> director as one to one (in reality 1-Many but did not get right with that)
#if false
            modelBuilder.Entity<Star>().
                HasOne(g=> g.Film)
                .WithMany(f => f.Stars)
                .HasForeignKey(g => g.FilmId)
                .IsRequired();


            modelBuilder.Entity<Genre>()
                .HasOne(g => g.Film)
                .WithMany(f => f.Genres)
                .HasForeignKey(g=>g.FilmId)
                .IsRequired();
#endif

            modelBuilder.Entity<Film>()
                .HasOne(f => f.User)
                .WithOne(u => u.Film)
                .IsRequired();

            modelBuilder.Entity<Film>()
                .HasOne(f => f.Director)
                .WithOne(u => u.Film)
                .IsRequired();




        }
    }
}
