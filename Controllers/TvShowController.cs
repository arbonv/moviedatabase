﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieDatabase.Data;
using MovieDatabase.Models;

namespace MovieDatabase.Controllers
{
    /**
     * Controller class for tv show
     */
    public class TvShowController : FilmController
    {

        public TvShowController(MovieContext context) : base(context) { }
      

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }
        public async Task<IActionResult> CreateTvShow(/*[Bind("Id,Title,Length,Director,ReleaseDate,Description,Rating,Genre")]*/ TvShow movie)
        {
            return await base.CreateFilm(movie);
        }



        [Authorize]
        new public async Task<IActionResult> Edit(int id)
        {
            return await base.Edit(id);            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> EditMovieDone(TvShow movie)
        {
            return await base.EditMovieDoneAsync(movie);
        }


        public async Task<IActionResult> ShowMovieDetails(int id)
        {
            return await base.ShowMovieDetail(id);

        }
    }
}