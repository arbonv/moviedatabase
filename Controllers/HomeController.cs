﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDatabase.Data;
using MovieDatabase.Models;

namespace MovieDatabase.Controllers
{
    /**
     * The home controller class
     */
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
        /**
         * This function lists all users from identity database
         * It doesn't mean that all users have created any movie
         */
        public async Task<IActionResult> ListUsers()
        {
            List<User> Users = new List<User>();
    
            List<Microsoft.AspNetCore.Identity.IdentityUser> lists =await _context.Users.ToListAsync();
            foreach (var elem in lists)
            {
                Users.Add(new User { UserName = elem.UserName });
            }
            return View(Users);
        }
    }
}
