﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDatabase.Data;
using MovieDatabase.Models;

namespace MovieDatabase.Controllers
{
    /**
     * Abstract class containing common code for Movie and TvShow
     */
    public abstract class FilmController : Controller
    {
        protected readonly MovieContext _context;

        public FilmController(MovieContext context)
        {
            _context = context;
        }
        public string GetUserId
        {
            get
            {
                return User.Identity.Name;
            }
        }

        /**
         * Here we come when we have pressed edit done
         */
        internal async Task<ActionResult> EditMovieDoneAsync(Film film)
        {
            Film filmInDataBase = await _context.Films
                .Include(f => f.Director)
                .Include(f => f.Genres)
                .Include(f => f.Stars)
                .SingleAsync(f => f.FilmId == film.FilmId);
            //save edited film 
            _context.Entry(filmInDataBase).CurrentValues.SetValues(film);
            //save the edited genres
            await UpdateGenresAsync(film).ConfigureAwait(false);
            //save the edited director
            await UpdateDirectorAsync(film).ConfigureAwait(false);
            //save the edited stars
            await UpdateStarsAsync(film).ConfigureAwait(false);

            await _context.SaveChangesAsync().ConfigureAwait(false);
            return RedirectToAction("Index", "Home");
        }


        /**
         * save the new values for director in database
         */
        private async Task UpdateDirectorAsync(Film movie)
        {
            Director director = movie.Director;
            if (director != null)
            {
                Director dirInDb = await _context.Director.FindAsync(movie.FilmId);
                dirInDb.FilePath = director.FilePath;
                dirInDb.Birthday = director.Birthday;
                dirInDb.FirstName = director.FirstName;
                dirInDb.LastName = director.LastName;
                director.Film = movie;
                _context.Update(dirInDb);
            }

        }

        /**
         * save the new values for director in database
         */
        private async Task UpdateStarsAsync(Film movie)
        {
            foreach (Star star in movie.Stars)
            {
                Star inDbStar = await _context.Stars.FindAsync(star.StarId).ConfigureAwait(false);
                inDbStar.FirstName = star.FirstName;
                inDbStar.LastName = star.LastName;
                inDbStar.Birthday = star.Birthday;
                inDbStar.FilePath = star.FilePath;
                _context.Update(inDbStar);
            }
        }
        /**
         * save the new values for genres in database
         */
        private async Task UpdateGenresAsync(Film movie)
        {
            Film filminDb = await _context.Films.FindAsync(movie.FilmId).ConfigureAwait(false);
            foreach (Genre genre in movie.Genres)
            {
                Genre genreInDb = _context.Genres.Find(genre.GenreId);
                genre.Film = filminDb;
                genreInDb.Name = genre.Name;
                _context.Genres.Update(genreInDb);
            }
        }

        public async Task<IActionResult> CreateFilm(/*[Bind("Id,Title,Length,Director,ReleaseDate,Description,Rating,Genre")]*/ Film movie)
        {
            bool isModelValid = ModelState.IsValid;
            if (isModelValid)
            {
                string userId = GetUserId;
                movie.User = new User { UserName = userId};
                //Prevent user to add same movie twice
                List<Film> films = await
                    _context.Films.Include(f => f.Director).
                    Select(f => f)
                    .Where(f => f.User.UserId.Equals(GetUserId))
                    .ToListAsync();
                int howManySameFilmForCurrentUser = films
                    .Select(f => f)
                    .Where(f => f.Title.Equals(movie.Title) && f.Director.FirstName.Equals(movie.Director.FirstName, StringComparison.OrdinalIgnoreCase)
&& f.Director.LastName.Equals(movie.Director.LastName, StringComparison.OrdinalIgnoreCase) && f.Director.Birthday.Equals(movie.Director.Birthday))
                    .ToList().Count;
                //user did not create same film before add
                if (howManySameFilmForCurrentUser == 0)
                {
                    _context.Add(movie);
                    await _context.SaveChangesAsync();
                    return View("../Home/Index");
                }
                String errorMessage = "The movie was already created by you, go back and edit or rate it";
                ViewBag["ErrorMessage"] = errorMessage;
                //user has created the same film before, inform that the film already exists
                return View("Error");

            }
            if(movie.GetType() == typeof(Movie))
            {
                return View("../Movies/Create");
            }
            return View("../TvShow/Create");
        }


        /**
         * Handle edit logic for both movie and tv show
         */
        internal async Task<IActionResult> Edit(int id)
        {
            Film filmInDb = await _context.Films.
                Include(f => f.Director)
                .Include(f => f.User).Include(f => f.Stars).Include(f => f.Genres)
                .Select(f => f).Where(f => f.FilmId == id)
                .FirstOrDefaultAsync();
            //fetch all movies for current user

            List<Film> filmsForThisUser = await _context.Films
                .Include(f => f.User).Include(f => f.Stars).Include(f => f.Director).Include(f => f.Genres)
                .Select(f => f).Where(f => f.User.UserName.Equals(GetUserId)).ToListAsync().ConfigureAwait(false);

            Film filmForThisUser = filmsForThisUser.Select(f => f)
                 .Where(f => f.Title.Equals(filmInDb.Title) &&
                f.Director.FirstName.Equals(filmInDb.Director.FirstName, StringComparison.OrdinalIgnoreCase)
                && f.Director.LastName.Equals(filmInDb.Director.LastName, StringComparison.OrdinalIgnoreCase)
                && f.Director.Birthday.Equals(filmInDb.Director.Birthday)).FirstOrDefault();


            //The user has not created this film, he can rate but not modify
            if (filmForThisUser == null)
            {

                ViewData["ErrorMessage"] = "You can not edit something not ccreated by you. " +
                                    "Rate the film and after that you can edit it";
                return View("../Movies/Error");
            }

            if (filmForThisUser.GetType() == typeof(Movie))
            {
                Movie movie = (Movie)filmForThisUser;
                return View("../Movies/Edit", movie);
            }
            else if (filmForThisUser.GetType() == typeof(TvShow))
            {
                TvShow tvShow = (TvShow)filmForThisUser;
                return View("../TvShow/Edit", tvShow);
            }
            return NotFound();
        }

        //Calculate average for same film
        protected async Task<double> CalculateAverageRatio(Film film)
        {
            //choose all films
            var allFilms = await _context.Films.Include(f => f.Director).Select(f => f).ToListAsync().ConfigureAwait(false);
            //choose all film which are equal to our movie 
            List<Film> sameFilm = allFilms

                .Where(f => f.Title.Equals(film.Title) && f.Director.FirstName.Equals(film.Director.FirstName, StringComparison.OrdinalIgnoreCase)
 && f.Director.LastName.Equals(film.Director.LastName, StringComparison.OrdinalIgnoreCase) && f.Director.Birthday.Equals(film.Director.Birthday))

                .Select(f => f).ToList();
            //calculate average of ratio for all users
            double value = sameFilm.Select(x => x.Rating).Sum() / sameFilm.Count;
            //show only two decimals
            value = Math.Round(value, 2);
            return value;
        }

        /**
         * Show movie details */
        internal async Task<IActionResult> ShowMovieDetail(int id)
        {

            Film film = await _context.Films
               .Include(c => c.Director).
               Include(c => c.Stars).
               Include(c => c.User).
               Include(c => c.Genres).
               SingleOrDefaultAsync(x => x.FilmId == id).
               ConfigureAwait(false);

            if (film == null)
            {
                return NotFound();
            }
            //calculate average rate before showing
            film.AverageRate = await CalculateAverageRatio(film);
            //check if it is movie or tv show and show corresponding ui
            if (film.GetType() == typeof(Movie))
            {
                Movie movie = (Movie)film;
                return View("../Movies/ShowMovieDetails", movie);
            }
            else if (film.GetType() == typeof(TvShow))
            {
                TvShow tvShow = (TvShow)film;
                return View("../TvShow/ShowMovieDetails", film);
            }

            return NotFound();
        }

        /*
         * The user has pressed the submit button when he/he has rated the film
         */

        public async Task<IActionResult> SubmitRatingAsync(Film film)
        {
            int id = film.FilmId;
            int newRatio = film.Rating;
            //fetch the entire film from db
            var filmInDb = await _context.Films
                .Include(f => f.Director)
                .Include(f => f.Genres)
                .Include(f => f.User)
                .Include(f => f.Stars)
                .Select(f => f)
                .Where(f => f.FilmId == film.FilmId).
                FirstOrDefaultAsync();

            var allFilmsForUser = await _context.Films
                .Include(f => f.Director)
                .Include(f => f.User)
                .Select(f => f).ToListAsync().ConfigureAwait(false);

            allFilmsForUser = allFilmsForUser.Select(f => f)
             .Where(f => f.Title.Equals(filmInDb.Title) &&
             f.Director.FirstName.Equals(filmInDb.Director.FirstName, StringComparison.OrdinalIgnoreCase)
             && f.Director.LastName.Equals(filmInDb.Director.LastName, StringComparison.OrdinalIgnoreCase)
             && f.Director.Birthday.Equals(filmInDb.Director.Birthday))
             .ToList();

            //check if we have the film from loged in user in database
            Film filmInDbFromLogedInUser = allFilmsForUser
                .Select(f => f)
                .Where(f => f.User.UserName.Equals(GetUserId))
                .ToList().FirstOrDefault();


            //no user for this film 
            if (filmInDbFromLogedInUser == null)
            {
                //The user has not created this film create same film with but with the new rating for the user and save to the database
                await CreateFilmForThisUserAsync(filmInDb, newRatio);
            }
            // the user has created this film before update the old rating value to new rating
            else
            {
                filmInDbFromLogedInUser.Rating = newRatio;
                _context.Films.Update(filmInDbFromLogedInUser);


            }
            //Save changes
            await _context.SaveChangesAsync().ConfigureAwait(false);
            //send user back to index
            return RedirectToAction("Index", "Home");

        }
        /**
         * Helper method to create the film
         */
        private async Task CreateFilmForThisUserAsync(Film film, int newRatio)
        {
            Film newFilm = null;
            if (film.GetType() == typeof(Movie))
            {
                newFilm = new Movie();
            }
            else if (film.GetType() == typeof(TvShow))
            {
                newFilm = new TvShow();

            }
            //The user is current user
            newFilm.User = new User
            {
                UserName = GetUserId,
                //assign navigation property
                Film = newFilm
            };
            newFilm.Rating = newRatio;
            //copy all members from  the old film we want to rate
            CopyNeceserryMembersFromExistingFilm(newFilm, film);
            await _context.Films.AddAsync(newFilm).ConfigureAwait(false);

        }

        private async void CopyNeceserryMembersFromExistingFilm(Film newFilm, Film film)
        {
            // fetch the complete film from database and assign all values except the ratio to to the new user
            Film filmInDb = await _context.Films
                .Include(f => f.Director).Include(f => f.Stars).Include(f => f.Genres).
                Select(f => f).
                Where(f => f.FilmId == film.FilmId).
                FirstOrDefaultAsync().ConfigureAwait(false);
            //assign all others values from film in db
            newFilm.Title = filmInDb.Title;
            newFilm.Length = filmInDb.Length;
            newFilm.Director = new Director
            {
                FirstName = filmInDb.Director.FirstName,
                LastName = filmInDb.Director.LastName,
                Birthday = filmInDb.Director.Birthday,
                Film = newFilm,
            };
            newFilm.Description = filmInDb.Description;
            newFilm.Length = filmInDb.Length;
            newFilm.ReleaseDate = filmInDb.ReleaseDate;
            newFilm.YoutubePreviewLink = filmInDb.YoutubePreviewLink;
            List<Genre> genres = new List<Genre>();
            foreach (Genre g in filmInDb.Genres)
            {
                Genre genre = new Genre
                {
                    Name = g.Name,
                    Film = newFilm
                };
                genres.Add(genre);
            }
            newFilm.Genres = genres;
            List<Star> stars = new List<Star>();
            foreach (Star st in filmInDb.Stars)
            {
                Star star = new Star
                {
                    FirstName = st.FirstName,
                    LastName = st.LastName,
                    Film = newFilm
                };
                stars.Add(star);
            }
            newFilm.Stars = stars;
            if (newFilm.GetType() == typeof(TvShow) && filmInDb.GetType() == typeof(TvShow))
            {
                ((TvShow)newFilm).StartYear = ((TvShow)filmInDb).StartYear;
                ((TvShow)newFilm).EndYear = ((TvShow)filmInDb).EndYear;
                ((TvShow)newFilm).NumberOfSeasons = ((TvShow)filmInDb).NumberOfSeasons;
            }

        }

    }
}
