﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDatabase.Data;
using MovieDatabase.Models;
using MovieDatabase.ViewModels;

namespace MovieDatabase.Controllers
{
    public class MoviesController : FilmController
    {
        /**
         * Controller class for movie
         */

        public MoviesController(MovieContext context) : base(context)
        {
            
        }

        // GET: Movies
        public async Task<IActionResult> Index()
        {
            List<Film> films = await _context.Films.Select(m => m).ToListAsync();
            foreach (Film film in films)
            {
                film.AverageRate = await CalculateAverageRatio(film);
            }

            return View(films);
        }
        [HttpPost]
        public async Task<IActionResult> Index(string filmName)
        {
            var items = await _context.Films.Select(m => m).Where(m => m.Title.Contains(filmName)).ToListAsync().ConfigureAwait(false);
            foreach(Film film in items)
            {
                film.AverageRate = await CalculateAverageRatio(film);
            }
            return View(items);
        }

       [Authorize]
        // GET: Movies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateMovie(/*[Bind("Id,Title,Length,Director,ReleaseDate,Description,Rating,Genre")]*/ Movie movie)
        {
            return await base.CreateFilm(movie);
        }

        // GET: Movies/Edit/5
        [Authorize]
        new public async Task<IActionResult> Edit(int id)
        {
            return await base.Edit(id);
            
        }
        public async Task<IActionResult> ListAllMoviesForGenre(int id)
        {
            Genre genre = await _context.Genres.FindAsync(id);
            
            if(genre == null)
            {
                return NotFound();
            }
            List<Film> listWithFilms =await  _context.Genres
                .Where(g=>  g.Name.Equals(genre.Name))
                .Select(i => i.Film).ToListAsync().ConfigureAwait(false);
            List<Film> listWithFilmsDirectors = new List<Film>();
            GenreAndMoviesViewModel viewModel = new GenreAndMoviesViewModel
            {
                MovieGenrie = genre,
                Movies = listWithFilms
            };
            return View(viewModel);
        }

        [Authorize]
        public async Task<IActionResult> ListAllMoviesForStar(int id)
        {
            Role star = await _context.Stars.FindAsync(id);
            if (star == null)
            {
                return NotFound();
            }
            List<Film> listWithFilmsForStar = await _context.Stars
                .Where(s => s.FirstName.Equals(star.FirstName) && s.LastName.Equals(star.LastName) && s.Birthday.Equals(star.Birthday))
                .Select(s => s.Film)
                .ToListAsync();

            RoleAndMoviesViewModel roleAndMoviesViewModel = new RoleAndMoviesViewModel
            {
                MovieRole = star,
                Movies = listWithFilmsForStar
            };
            return View("ListAllMoviesForRole", roleAndMoviesViewModel);
        }
        [Authorize]
        public async Task<IActionResult> RateFilm(int id)
        {
            Film film = await _context.Films.FindAsync(id);
            if(film == null)
            {
                return NotFound();
            }
            return View(film);
        }
        [Authorize]
        [HttpPost]
        public  async Task<IActionResult> SubmitRating(Movie film)
        {
          return await  base.SubmitRatingAsync(film).ConfigureAwait(false);
        }

        public IActionResult AddMovie()
        {
            return View();
        }

        public async Task<IActionResult> ShowMovieDetails(int id)
        {
            return await base.ShowMovieDetail(id).ConfigureAwait(false);
           
        }


        public async Task<IActionResult> ListAllMoviesForDirector(int id)
        {
            Role director = await _context.Director.FindAsync(id);
            if (director == null)
            {
                return NotFound();
            }
            List<Film> listWithFilmsForStar = await _context.Director
                .Where(d => d.FirstName.Equals(director.FirstName) && d.LastName.Equals(director.LastName) && d.Birthday.Equals(director.Birthday))
                .Select(s => s.Film)
                .ToListAsync();

            RoleAndMoviesViewModel roleAndMoviesViewModel = new RoleAndMoviesViewModel
            {
                MovieRole = director,
                Movies = listWithFilmsForStar
            };
            return View("ListAllMoviesForRole", roleAndMoviesViewModel);
        }

        //Show all movies and tv shown in database
        public async Task<IActionResult> MoviesAndTvShown()
        {
            List<Film> allFilms = await _context.Films.Select(m => m).Include(C => C.Director).Include(c =>c.Genres).Include(c=>c.Stars).Include(C=>C.User).ToListAsync().ConfigureAwait(false);
            //do not show duplicates e.g same film created or rated by more than one user
            HashSet<Film> films = new HashSet<Film>(allFilms);
            List<Movie> movies = films.OfType<Movie>().ToList();
            List<TvShow> tvShows = films.OfType<TvShow>().ToList();
            FilmViewModel filmViewModel = new FilmViewModel() { Movies = movies, TvShows = tvShows };
            return View(filmViewModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> EditMovieDone(Movie movie)
        {
           return await base.EditMovieDoneAsync(movie).ConfigureAwait(false);
        }
    }
}
