﻿using MovieDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDatabase.ViewModels
{
    public class GenreAndMoviesViewModel
    {
        public Genre MovieGenrie { get; set; }
        public List<Film> Movies { get; set; }
    }
}
