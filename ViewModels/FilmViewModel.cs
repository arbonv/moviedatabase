﻿using MovieDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDatabase.ViewModels
{
    public class FilmViewModel
    {
        public List<Movie> Movies { get; set; }

        public List<TvShow> TvShows { get; set; }

    }
}
