﻿using FoolProof.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace MovieDatabase.Models
{
    //Class for tv show
    public class TvShow : Film
    {
        [Range(1, 30, ErrorMessage ="Value must be between 1 to 30")]

        public int NumberOfSeasons { get; set; }

        [Required(ErrorMessage  = "Enter a value between 1900 and 2040")]
        [Range(1900, 2020)]
        public int StartYear { get; set; }
        [Range(1900, 2020, ErrorMessage = "Enter a value between 1900 and 2040")]
        [GreaterThan("StartYear")]
        public int EndYear { get; set; }


    }

}
