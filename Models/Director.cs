﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieDatabase.Models
{
    public class Director : Role
    {

        [Key]
        public int DirectorId { get; set; }


        public override string ToString() => "Directorname: " + FirstName + " " + LastName;


        public override int GetHashCode()
        {
            return string.Format("{0}_{1}_{2}", FirstName, LastName, Birthday.ToString()).GetHashCode();
        
       }
        public int FilmId { get; set; }

    }
}
