﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace MovieDatabase.Models
{
    /// <summary>
    /// Abstract class describing a role in a movie
    /// The role can be either a star or a director
    /// 
    /// </summary>
    public abstract class Role
    {
        private string filePath;

        
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        [Required]
        [StringLength(20)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        [Required]
        [StringLength(20)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name ="Birth day")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime Birthday { get; set; }


        [NotMapped]
        public string  FilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                filePath = value;
                if(filePath != null && File.Exists(filePath))
                {
                    Image = File.ReadAllBytes(value);
                }
                
            }
        }


        public byte[] Image { get; set; }

        
        //Navigation propery for Film
        public virtual Film Film { get; set; }


        public override bool Equals(object obj)
        {
            if (obj == null || !GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Role role = (Role)obj;
                return FirstName.Equals(role.FirstName, StringComparison.OrdinalIgnoreCase)
                    && LastName.Equals(role.LastName, StringComparison.OrdinalIgnoreCase)
                    && Birthday.Equals(role.Birthday);
            }
        }
        

        public override int GetHashCode()
        {
            return string.Format("{0}_{1}_{2}", FirstName, LastName, Birthday.ToString()).GetHashCode();
        }
    }
}
