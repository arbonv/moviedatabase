﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MovieDatabase.Models
{
    /**
     * Abstract class defining a film. Both Movie and TvShow inherit common fuctionality from this class
     */
    public abstract class Film
    {
        private string filePath;

        [Key]
        public int FilmId { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 4)]
        public string Title { get; set; }

        [Range(25, 240)]
        [Required]
        [Display(Name ="Length in min")]
        public int Length { get; set; }
        [Required]
        public Director Director { get; set; }
        

        [Required]
        public List<Star> Stars { get; set; }

        [Display(Name = "Release Date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime ReleaseDate { get; set; }

        
        [Required]
        public string Description { get; set; }

        [Required]
        [Range(1, 10)]
        public int Rating { get; set; }

        [Display(Name ="Average rating")]
        [NotMapped]
        public double AverageRate { get; set; }

        
        [Required]
        public List<Genre> Genres { get; set; }

        [NotMapped]
        public string FilePath
        {
            get
            {
                return filePath;
            }
             set
            {
                filePath = value;
                if(filePath != null && File.Exists(filePath))
                {
                    Image = File.ReadAllBytes(filePath);
                }
                
            }
        }

        //the moview image
        
        public byte[] Image { get; set; }

        [StringLength(50, MinimumLength = 4)]
        [Required]
        [Url(ErrorMessage="Invalid url")]
        public string YoutubePreviewLink { get; set; }

        public User User { get; set; }

        //two films are considered to be same if they have same title and same director
        //Did not work so i tested it "manually"
        public override bool Equals(object obj)
        {
            if (obj == null || !GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Film film = (Film)obj;
                return Title.Equals(film.Title, StringComparison.OrdinalIgnoreCase);
            }
        }

        public override int  GetHashCode()
        {
            return Title.GetHashCode() + Director.GetHashCode();

        }


        /// <summary>
        /// Validates a URL.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private bool ValidateUrl(string url)
{
    Uri validatedUri;

    if (Uri.TryCreate(url, UriKind.Absolute, out  validatedUri)) //.NET URI validation.
    {
        //If true: validatedUri contains a valid Uri. Check for the scheme in addition.
        return (validatedUri.Scheme == Uri.UriSchemeHttp || validatedUri.Scheme == Uri.UriSchemeHttps);
    }
    return false;
}
    }
}
