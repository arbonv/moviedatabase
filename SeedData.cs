﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MovieDatabase.Data;
using MovieDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieDatabase
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MovieContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MovieContext>>()))
            {
                if(context.Films.Any())
                {
                    return; //films exist in database
                }
                
                Movie movie = GetMovie();
                TvShow tvShow = GetTvShow();
                context.Films.Add(movie);
                context.Films.Add(tvShow);
                Movie movie1 = GetMovie();
                movie1.Title += "2";
                context.Films.Add(movie1);
                context.SaveChanges();
            }
        }

        private static TvShow GetTvShow()
        {
            List<Genre> genres = new List<Genre>();
            List<Star> stars = new List<Star>();
            stars.Add(new Star
            {
                FirstName = "Chuck",
                LastName = "Norris",
                Birthday = new DateTime(1988, 03, 15)
            });
            stars.Add(new Star
            {
                FirstName = "Bruce",
                LastName = "Lee",
                Birthday = new DateTime(1938, 03, 15)
            });
            stars.Add(new Star
            {
                FirstName = "Angelina",
                LastName = "Jolie",
                Birthday = new DateTime(1928, 03, 25)
            });
            genres.Add(new Genre { Name = "thriller tvShow" });
            genres.Add(new Genre { Name = "Action tvShow" });
            genres.Add(new Genre { Name = "Action" });
            TvShow  tvShow = new TvShow()
            {
                User = new User { UserName = "arbonv@gmail.com" },
                Director = new Director
                {
                    FirstName = "Arbon",
                    LastName = "Vata",
                    Birthday = new DateTime(2014, 03, 10)
                },
                Length = 150,
                ReleaseDate = new DateTime(1968, 03, 16),

                Rating = 4,
                Description = "amazing movie from arbon",

                Title = "Arbons tv show",
                YoutubePreviewLink = @"https://www.youtube.com/watch?v=aL1NpdobyHY",
                StartYear = 1998,
                EndYear = 2014,
                NumberOfSeasons = 13
                
            };
            tvShow.Stars = stars;
            tvShow.Genres = genres;
            return tvShow;
        }

        private static Movie GetMovie()
        {
            Movie movie = new Movie()
            {
                User = new User { UserName = "arbon.vata@gmail.com" },
                Director = new Director
                {
                    FirstName = "A",
                    LastName = "Vata",
                    Birthday = new DateTime(2014, 03, 10),

                },
                Length = 150,
                ReleaseDate = new DateTime(1978, 02, 16),

                Rating = 4,
                Description = "Balblabla",
                Title = "Movie",

                YoutubePreviewLink = @"https://www.youtube.com/watch?v=aL1NpdobyHY",

            };
            var starsShow = new List<Star>();
            var genresShow = new List<Genre>();
            genresShow.Add(new Genre { Name = "Drama" });
            genresShow.Add(new Genre { Name = "Comedy" });
            genresShow.Add(new Genre { Name = "Action" });

            starsShow.Add(new Star
            {
                FirstName = "Will",
                LastName = "Smith",
                Birthday = new DateTime(1988, 03, 15)

            });
            starsShow.Add(new Star
            {
                FirstName = "Bruce",
                LastName = "Lee",
                Birthday = new DateTime(1938, 03, 15)
            });
            starsShow.Add(new Star
            {
                FirstName = "Angelina",
                LastName = "Jolie",
                Birthday = new DateTime(1928, 03, 25)
            });
            movie.Genres = genresShow;
            movie.Stars = starsShow;
            return movie;
        }
    }
}
